BrowserPolicy.content.allowOriginForAll( 's3.amazonaws.com' );
BrowserPolicy.content.allowOriginForAll( 'placehold.it' );
BrowserPolicy.content.allowOriginForAll( 'placeholdit.imgix.net' );
BrowserPolicy.content.allowOriginForAll( 'fonts.googleapis.com' );
BrowserPolicy.content.allowOriginForAll( 'fonts.gstatic.com' );
