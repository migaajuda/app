import { Mongo } from 'meteor/mongo';
import faker from 'faker';

export const PedidoAjuda = new Mongo.Collection('PedidoAjuda');

PedidoAjuda.schema = new SimpleSchema({
  title: {
    type: String,
    label: 'O título do documento.',
  },
  title: {
    type: String,
    label: 'O título do documento.',
  },
  images: {
    type: String,
    label: 'O título do documento.',
  },
});

PedidoAjuda.attachSchema(PedidoAjuda.schema);

Factory.define('pedidoajuda', PedidoAjuda, {
  title: () => faker.hacker.phrase(),
});
