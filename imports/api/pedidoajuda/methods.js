import { PedidoAjuda } from './pedidoajuda';

export const insertDocument = new ValidatedMethod({
  name: 'pedidoajuda.insert',
  validate: new SimpleSchema({
    title: { type: String },
  }).validator(),
  run(document) {
    PedidoAjuda.insert(document);
  },
});

export const updateDocument = new ValidatedMethod({
  name: 'documents.update',
  validate: new SimpleSchema({
    _id: { type: String },
    'update.title': { type: String, optional: true },
  }).validator(),
  run({ _id, update }) {
    PedidoAjuda.update(_id, { $set: update });
  },
});

export const removeDocument = new ValidatedMethod({
  name: 'documents.remove',
  validate: new SimpleSchema({
    _id: { type: String },
  }).validator(),
  run({ _id }) {
    PedidoAjuda.remove(_id);
  },
});
