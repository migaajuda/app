import { composeWithTracker } from 'react-komposer';
import { AppActionbar } from '../components/app-actionbar';

const composer = (props, onData) => {
  onData(null, { hasUser: Meteor.user() });
};

export default composeWithTracker(composer, {}, {}, { pure: false })(AppActionbar);
