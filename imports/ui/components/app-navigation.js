import React from 'react';
import { Navbar } from 'react-bootstrap';
import { Link } from 'react-router';
import { PublicNavigation } from './public-navigation';
import { AuthenticatedNavigation } from './authenticated-navigation';

export class AppNavigation extends React.Component {
  renderNavigation(hasUser) {
    return hasUser ? <Navbar.Collapse><AuthenticatedNavigation /></Navbar.Collapse> : false;
  }

  renderToggle(hasUser){
    return hasUser ? <Navbar.Toggle /> : false;
  }

  render() {
    return <Navbar id="navbar">
      <Navbar.Header>
        <Navbar.Brand>
          <Link to="/">Miga Ajuda</Link>
        </Navbar.Brand>
        { this.renderToggle(this.props.hasUser) }
      </Navbar.Header>
      { this.renderNavigation(this.props.hasUser) }
    </Navbar>;
  }
}

AppNavigation.propTypes = {
  hasUser: React.PropTypes.object,
};
