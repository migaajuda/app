import React from 'react';
import { AuthenticatedNavigation } from './authenticated-actionbar';

export class AppActionbar extends React.Component {
  renderActionbar(hasUser) {
    return hasUser ? <AuthenticatedNavigation /> : false;
  }

  render() {
    return <div className="actionbar">
      { this.renderActionbar(this.props.hasUser) }
    </div>;
  }
}

AppActionbar.propTypes = {
  hasUser: React.PropTypes.object,
};
