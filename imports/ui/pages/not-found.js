import React from 'react';
import { Alert } from 'react-bootstrap';

export const NotFound = () => (
  <Alert bsStyle="danger">
    <p><strong>Erro [404]</strong>: Parece que "{ window.location.pathname }" não existe.</p>
  </Alert>
);
