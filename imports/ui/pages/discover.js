import React from 'react';
import { Row, Col } from 'react-bootstrap';
import DocumentsList from '../containers/documents-list.js';
import { AddDocument } from '../components/add-document.js';

export const Discover = () => (
  <Row>
    <Col xs={ 12 }>
      <h4>Discover</h4>
      <AddDocument />
      <DocumentsList />
    </Col>
  </Row>
);
