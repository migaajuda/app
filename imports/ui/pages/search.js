import React from 'react';
import { Row, Col } from 'react-bootstrap';
import DocumentsList from '../containers/documents-list.js';
import { SearchFriends } from '../components/search-friends.js';

export const Search = () => (
  <Row>
    <Col xs={ 12 }>
      <SearchFriends />
    </Col>
  </Row>
);
