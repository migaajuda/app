import React from 'react';
import { Jumbotron } from 'react-bootstrap';

export const Index = () => (
  <Jumbotron className="text-center">
    <h2>Miga Ajuda</h2>
    <p>Um ponto de entrada para solicitações de ajuda</p>
  </Jumbotron>
);
